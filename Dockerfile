FROM node:8

WORKDIR /app

COPY *.json *.js /app/
COPY public /app/public
COPY views /app/views


RUN npm install

EXPOSE 80

CMD [ "npm" , "start" ]

