# Docker Node + MySQL


1. Run mysql v5.7 container from docker image

> ```docker run --name YOUR_MYSQL_NAME -d -e MYSQL_ROOT_PASSWORD=YOUR_PASSWORD mysql:5.7```

2. You can check if  mysql container is running

> ```docker ps -a```

![P1](./images/1.png)

3. Now , we create our node application using ```Dockerfile```

```
FROM node:8

WORKDIR /app

COPY *.json *.js /app/
COPY public /app/public
COPY views /app/views


RUN npm install

EXPOSE 80

CMD [ "npm" , "start" ]


```

4. Before we can connect our node app to our mysql database, we can check ip of database server
using 

> ```docker network inspect bridge```

```
"Containers": {
            "9b283abfcbf6931225194c17c65981159165db0a7be80307387a84fcd39f579d": {
                "Name": "mql1",
                "EndpointID": "5aeed766fca374e2829bf10a681b2e9f7c548374160cf7d7824954f9c409d451",
                "MacAddress": "02:42:ac:11:00:02",
                "IPv4Address": "172.17.0.2/16",
                "IPv6Address": ""
            }
        },

```

5. Now, we can connect our node app

```
const sequelize = new Sequelize('test_db1','arief','arief',{
    host:'172.17.0.2',
    port:3306,
    dialect:'mysql'
})
```

6. Build your own docker image

> ```docker build -t YOUR_NODE_IMAGE_NAME PATH_OF_DOCKERFILE```

7. Run your node app container

> ```docker run -d -p WHATEVER_PORT_OF_YOUR_HOST:80 --name YOUR_CONTAINER_NAME YOUR_NODE_IMAGE_NAME ```





