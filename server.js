const express = require('express')
const app =express()


const Sequelize = require('sequelize')
const sequelize = new Sequelize('test_db1','arief','arief',{
    host:'172.17.0.2',
    port:3306,
    dialect:'mysql'
})


const personModel = sequelize.define('person',{
    id:{
        type:Sequelize.INTEGER,
        primaryKey:true,
        autoIncrement:true
    },
    name:{
        type:Sequelize.STRING(25)
    }
},{
    tableName:'person',
    timestamps:false
})

sequelize.sync()


app.set('view engine','pug')


app.get('/',(req,res)=>{
    res.sendFile(__dirname + '/public/index.html')
})

app.get('/list',(req,res)=>{
    personModel.findAll()
        .then(v=> {
            res.render('list',{
                dPerson:v
            })
        })
        .catch(err=>{
            console.error(err)
        })
})

app.listen(80 , ()=> console.log('server running....'))